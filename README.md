# TodosClient

## Routes

- Auth
  - Login
  - Signup
  - Reset Password
- Todos
  - View All for User
  - Todo Details
  - Edit Todo

## Data Models

### User

| Field    | Type   |
| -------- | ------ |
| username | string |
| password | string |
| userId   | string |

### Todo

| Field             | Type    |
| ----------------- | ------- |
| todoId            | string  |
| userId            | string  |
| title             | string  |
| description       | string  |
| createdDateTime   | Date    |
| status            | string  |
| isComplete        | boolean |
| completedDateTime | Date    |

## API Operations

### Todos

| Operation          | Method   | Endpoint                                      |
| ------------------ | -------- | --------------------------------------------- |
| getTodosForUser    | `GET`    | /api/v1/todos/get/all?user={userId}           |
| getTodos           | `GET`    | /api/v1/todos/get?user={userId}&todo={todoId} |
| createTodo         | `POST`   | /api/v1/todos/create                          |
| updateTodoByTodoId | `PUT`    | /api/v1/todos/update                          |
| deleteTodoByTodoId | `DELETE` | /api/v1/todos/delete?todo={todoId}            |

### User

| Operation | Method | Endpoint       |
| --------- | ------ | -------------- |
| signup    | `POST` | /api/v1/signup |
| login     | `POST` | /api/v1/login  |
| logout    | `GET`  | /api/v1/logout |
