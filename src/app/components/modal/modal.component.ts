import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
})
export class ModalComponent {
  @Input()
  isModalOpen!: boolean;

  @Output()
  modalClosed = new EventEmitter<boolean>();

  onCloseModal() {
    this.modalClosed.emit(false);
  }
}
