import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { catchError } from 'rxjs';
import { faCircleXmark } from '@fortawesome/free-solid-svg-icons';
import { User } from 'src/app/models/user';
import { LoginService } from 'src/app/services/login-service';
import { HttpService } from 'src/app/services/http-service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  signupForm!: FormGroup;
  faCircleXmark = faCircleXmark;

  constructor(
    private loginService: LoginService,
    private httpService: HttpService,
    private router: Router
  ) {}

  onInputChange(fieldName: string) {
    console.log(fieldName);
  }

  onSubmit() {
    const newUser: User = {
      username: this.signupForm.value.username,
      password: this.signupForm.value.password,
    };

    this.loginService
      .signup(newUser)
      .pipe(catchError(this.httpService.handleHttpErrors))
      .subscribe({
        next: (response: any) => {
          console.log('response:', response);
          this.router.navigate(['login']);
        },
        error: (err: Error) => {
          console.error(err);
        },
        complete: () => {
          console.log('user signup operation complete');
          this.httpService.setXsrfToken();
        },
      });
  }

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      username: new FormControl(null),
      password: new FormControl(null),
    });
  }
}
