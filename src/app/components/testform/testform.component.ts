import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { getCookie } from 'typescript-cookie';
import { User } from 'src/app/models/user';
@Component({
  selector: 'app-testform',
  templateUrl: './testform.component.html',
  styleUrls: ['./testform.component.css'],
})
export class TestformComponent implements OnInit {
  testLoginForm!: FormGroup;

  constructor(private httpClient: HttpClient) {}

  validateLoginDetails() {
    window.sessionStorage.setItem(
      'userdetails',
      JSON.stringify({
        username: this.testLoginForm.value.username,
        password: this.testLoginForm.value.password,
        role: 'user',
      })
    );

    return this.httpClient.get('http://localhost:8080/api/v1/auth/login', {
      observe: 'response',
      withCredentials: true,
    });
  }

  onSubmit() {
    const user: User = {
      username: this.testLoginForm.value.username,
      password: this.testLoginForm.value.password,
      role: 'user',
    };

    this.validateLoginDetails().subscribe((response: any) => {
      console.log('response: ', response);
      // if (!getCookie('XSRF-TOKEN')) {
      //   throw new Error('Unable to get XSRF-TOKEN');
      // }
      let xsrf = getCookie('XSRF-TOKEN');
      window.sessionStorage.setItem('XSRF-TOKEN', xsrf!);
    });

    // this.httpClient
    //   // .post('http://localhost:8080/api/v1/auth/signup', user)
    //   .get('http://localhost:8080/api/v1/auth/login', {}) // GET works w/o CORS errs
    //   .subscribe({
    //     next: (data: any) => {
    //       console.log('data:', data);
    //     },
    //     error: (e: Error) => {
    //       console.error('an error occurred: ', e);
    //     },
    //     complete: () => {
    //       console.info('get operation complete');
    //     },
    //   });
    // console.log('user:', user);
  }
  ngOnInit(): void {
    this.testLoginForm = new FormGroup({
      username: new FormControl(null),
      password: new FormControl(null),
    });
  }
}
