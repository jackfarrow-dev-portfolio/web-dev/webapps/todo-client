import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription, catchError, pipe } from 'rxjs';
import { LoginService } from 'src/app/services/login-service';
import { HttpService } from 'src/app/services/http-service';
import { ValidatorService } from 'src/app/services/validator-service';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { getCookie } from 'typescript-cookie';
import { ValidationStyleMap } from 'src/app/models/validation-style-map';
import { faCircleXmark } from '@fortawesome/free-solid-svg-icons';
import { TodoClientForm } from 'src/app/models/todo-client-form';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy, TodoClientForm {
  loginForm!: FormGroup;
  subscription!: Subscription;
  faCircleXmark = faCircleXmark;
  validationStyles!: { [key: string]: ValidationStyleMap };
  errorMsgMap: { [key: string]: string } = { username: '*', password: '*' };
  formSubmitted = false;

  constructor(
    private loginService: LoginService,
    private httpService: HttpService,
    private validatorService: ValidatorService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  onInputChange(controlName: string): void {
    this.formSubmitted = false;
    this.validationStyles =
      this.validatorService.resetValidationStylesForControl(
        controlName,
        this.validationStyles
      );
  }

  setValidationStyles(): { [key: string]: ValidationStyleMap } {
    return this.validatorService.setValidationStylesForForm(
      ['username', 'password'],
      this.loginForm
    );
  }

  onSubmit(): void {
    this.formSubmitted = true;

    this.loginService.login({
      username: this.loginForm.value.username,
      password: this.loginForm.value.password,
    });

    this.validationStyles = this.setValidationStyles();
    this.errorMsgMap = this.validatorService.buildErrMsgMap(
      this.loginForm.controls
    );

    // .pipe(catchError(this.httpService.handleHttpErrors))
    // .subscribe({
    //   next: (response: any) => {
    //     console.log('response:', response);
    //     let xsrf = getCookie('XSRF-TOKEN');
    //     console.log(`xsrf: ${xsrf}`);
    //   },
    //   error: (err: Error) => {
    //     console.log('here is an error');
    //     console.error(err);
    //   },
    //   complete: () => {
    //     console.log('user login operation complete');
    //   },
    // });
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl(null, [
        Validators.required,
        Validators.minLength(8),
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(8),
      ]),
    });

    this.validationStyles = this.validatorService.buildValidationStyleMap([
      'username',
      'password',
    ]);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
