import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Todo } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo-service';

@Component({
  selector: 'app-edit-todo',
  templateUrl: './edit-todo.component.html',
  styleUrls: ['./edit-todo.component.css'],
})
export class EditTodoComponent implements OnInit {
  todo!: Todo;

  editForm!: FormGroup;

  constructor(private todoService: TodoService, private router: Router) {}

  cancelTodoEdit() {
    this.router.navigate(['login']);
  }
  onSubmit() {
    const updatedTodo: Todo = {
      todoId: this.todo.todoId,
      userId: this.todo.userId,
      title: this.editForm.value['title'],
      description: this.editForm.value['description'],
      status: this.editForm.value['status'],
      completedDateTime:
        this.editForm.value['status'] === 'COMPLETE'
          ? this.editForm.value['completedDateTime']
          : null,
      createdDateTime: this.todo.createdDateTime,
      isComplete: this.editForm.value['status'] === 'COMPLETE' ? true : false,
    };
    this.todoService.updateTodo(updatedTodo);
  }

  ngOnInit(): void {
    this.editForm = new FormGroup({
      title: new FormControl(null),
      description: new FormControl(null),
      status: new FormControl(null),
      completedDateTime: new FormControl(null),
    });

    if (this.todoService.selectedTodo) {
      this.todo = this.todoService.selectedTodo;
      console.log(this.todo);
      this.editForm.controls['title'].setValue(this.todo.title);
      this.editForm.controls['description'].setValue(this.todo.description);
      this.editForm.controls['status'].setValue(this.todo.status);
      if (this.todo.completedDateTime) {
        // TODO: Convert completedDateTime from Date object to string
        // in YYYY-MM-DD format for display in date picker field
        // on component load
        this.editForm.controls['completedDateTime'].setValue(
          this.todo.completedDateTime
        );
      }
    }
  }
}
