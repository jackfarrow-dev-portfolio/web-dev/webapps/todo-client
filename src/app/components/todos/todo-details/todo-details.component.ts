import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Todo } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo-service';
import {
  faPenToSquare,
  faTrash,
  faMagnifyingGlassPlus,
  faCircleXmark,
} from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-todo-details',
  templateUrl: './todo-details.component.html',
  styleUrls: ['./todo-details.component.css'],
})
export class TodoDetailsComponent {
  @Input()
  todo!: Todo;
  editMode = false;
  editForm!: FormGroup;
  faPenToSquare = faPenToSquare;
  faTrash = faTrash;
  faMagnifyingGlassPlus = faMagnifyingGlassPlus;
  faCircleXmark = faCircleXmark;
  displayFullDetails = false;

  constructor(private router: Router, private todoService: TodoService) {}

  toggleDisplayDetails(showDisplayDetails: boolean) {
    this.displayFullDetails = showDisplayDetails;
  }

  editTodo() {
    console.log(this.todo.todoId);
    this.todoService.setSelectedTodo(this.todo);
    this.router.navigate(['/todos/edit', this.todo.todoId]);
  }

  getDisplayDate(dateString: string) {
    try {
      const displayDate = new Date(dateString);
      return `${displayDate.getMonth()}/${displayDate.getDate()}/${displayDate.getFullYear()}`;
    } catch (err) {
      throw new Error(`Unable to parse date from ${dateString}`);
    }
  }

  getDisplayStatus(statusString: string) {
    let displayStatus = '';
    switch (statusString) {
      case 'NOT_STARTED':
        displayStatus = 'Not Started';
        break;
      case 'IN_PROGRESS':
        displayStatus = 'In Progress';
        break;
      case 'COMPLETED':
        displayStatus = 'Completed';
        break;
      default:
        displayStatus = 'Unknown';
    }
    return displayStatus;
  }

  getDisplayDescription(descString: string) {
    return descString.length > 100
      ? `${descString.substring(0, 100)}...`
      : descString;
  }

  ngOnInit() {}
}
