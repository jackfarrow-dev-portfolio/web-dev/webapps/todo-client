import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from } from 'rxjs';
import { TodoService } from 'src/app/services/todo-service';
import { Todo } from 'src/app/models/todo';
@Component({
  selector: 'app-todos-list',
  templateUrl: './todos-list.component.html',
  styleUrls: ['./todos-list.component.css'],
})
export class TodosListComponent implements OnInit, OnDestroy {
  username = 'App User';
  todos!: Todo[];
  todosListSubscription!: Subscription;
  todosNotStarted!: Todo[];
  todosInProgress!: Todo[];
  todosComplete!: Todo[];

  constructor(private todoService: TodoService) {}

  setTodoClassName(status: string): string {
    if (status === 'NOT_STARTED') return 'todo-list-not-start';
    if (status === 'IN_PROGRESS') return 'todo-list-in-progress';
    if (status === 'COMPLETE') return 'todo-list-completed';
    throw new Error('unknown status for todo');
  }
  ngOnInit(): void {
    const todosListForUser = from(
      this.todoService.getTodosByUserId(
        window.sessionStorage.getItem('currentUserId') || ''
      )
    );

    this.todosListSubscription = todosListForUser.subscribe({
      next: (todosList: Todo[]) => {
        // this.todos = todosList;
        this.todos = this.todoService.todos.filter(
          (todo: Todo) => todo.userId === 'e6db304f-07fa-40ba-8411-f1a330aad7bd'
        );
        this.todosNotStarted = this.todos.filter(
          (todo: Todo) => todo.status === 'NOT_STARTED'
        );

        this.todosInProgress = this.todos.filter(
          (todo: Todo) => todo.status === 'IN_PROGRESS'
        );

        this.todosComplete = this.todos.filter(
          (todo: Todo) => todo.status === 'COMPLETE'
        );
      },
      error: (err) => {
        console.error(err);
      },
      complete: () => {
        console.log('operation getTodosByUserId completed');
      },
    });
  }

  ngOnDestroy(): void {
    this.todosListSubscription.unsubscribe();
  }
}
