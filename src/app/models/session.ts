export interface Session {
  currentUserId: string;
  currentUserName: string;
}
