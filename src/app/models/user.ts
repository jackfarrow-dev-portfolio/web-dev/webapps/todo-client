export interface User {
  username: string;
  password: string;
  userId?: string;
  role?: string;
}
