export interface ValidationStyleMap {
  visibility: string;
  inputClasses: string;
  iconClasses: string;
  msgClasses: string;
}
