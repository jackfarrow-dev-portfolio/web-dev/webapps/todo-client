export interface Todo {
  todoId?: string;
  userId?: string;
  title: string;
  description: string;
  createdDateTime: string;
  status: 'NOT_STARTED' | 'IN_PROGRESS' | 'COMPLETE';
  isComplete: boolean;
  completedDateTime?: string;
}
