import { ValidationStyleMap } from './validation-style-map';

export interface TodoClientForm {
  onInputChange: (controlName: string) => void;
  setValidationStyles: () => { [key: string]: ValidationStyleMap };
  onSubmit: () => void;
}
