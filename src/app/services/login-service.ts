import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import HTTP_CONFIG from '../config/app-constants';

const { baseUrl, apiPath, loginRoute, signupRoute } = HTTP_CONFIG;
@Injectable()
export class LoginService {
  constructor(private httpClient: HttpClient) {}

  login(user: User): Observable<Object> {
    window.sessionStorage.setItem('userDetails', JSON.stringify(user));
    const url = `${baseUrl}/${apiPath}/${loginRoute}`;
    // console.log(`url: ${url}`); // OK
    return this.httpClient.get(url, {
      observe: 'response',
      withCredentials: true,
    });
  }

  signup(signupRequest: User): Observable<Object> {
    const url = `${baseUrl}/${apiPath}/${signupRoute}`;
    return this.httpClient.post(url, signupRequest, {
      observe: 'response',
      withCredentials: true,
    });
  }
}
