import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { ValidationStyleMap } from '../models/validation-style-map';

const VALID = 'VALID';
const VALIDATION_CLASSES = {
  hidden: 'hidden',
  visible: 'visible',
  inputErr: 'input-error',
  iconHidden: 'form-group-icon hidden',
  iconVisible: 'form-group-icon visible',
  msgHidden: 'form-validation-message error-message hidden',
  msgVisible: 'form-validation-message error-message visible',
};

@Injectable()
export class ValidatorService {
  /*TODO: Come up with a better name for this function */
  isFormFieldValid(formGroup: FormGroup, formFieldName: string) {
    if (!formGroup.controls[formFieldName]) {
      throw new Error(`Field ${formFieldName} does not exist for this form`);
    }
    const controls = formGroup.controls[formFieldName];
    // If form is submitted untouched without required/
    // min-length field satisfied
    if (formGroup.pristine === true && controls.errors !== null) {
      return false;
    }
    // If form is submitted touched and form field error exists
    if (formGroup.pristine === false && controls.errors !== null) {
      return false;
    }
    // If form is submitted untouched and no form field error exists
    if (formGroup.pristine === false && controls.errors === null) {
      return true;
    }
    // If form is submitted and both field and form status valid
    if (formGroup.status === VALID && controls.status === VALID) {
      return true;
    }
  }

  buildValidationStyleMap(formFieldNames: string[]): {
    [key: string]: ValidationStyleMap;
  } {
    const validationStyleMap: { [key: string]: ValidationStyleMap } = {};
    for (const n of formFieldNames) {
      validationStyleMap[n] = {
        visibility: VALIDATION_CLASSES.hidden,
        inputClasses: '',
        iconClasses: VALIDATION_CLASSES.iconHidden,
        msgClasses: VALIDATION_CLASSES.msgHidden,
      };
    }
    return validationStyleMap;
  }

  resetValidationStylesForControl(
    controlName: string,
    validationStyles: { [key: string]: ValidationStyleMap }
  ): { [key: string]: ValidationStyleMap } {
    const validationStylesCopy = {
      ...validationStyles,
      [controlName]: {
        visibility: VALIDATION_CLASSES.hidden,
        inputClasses: '',
        iconClasses: VALIDATION_CLASSES.iconHidden,
        msgClasses: VALIDATION_CLASSES.msgHidden,
      },
    };
    return validationStylesCopy;
  }

  setValidationStylesForForm(
    formFieldNames: string[],
    formGroup: FormGroup
  ): { [key: string]: ValidationStyleMap } {
    const validationStyles: { [key: string]: ValidationStyleMap } = {};

    for (const n of formFieldNames) {
      let styleObj: ValidationStyleMap = {
        visibility: this.isFormFieldValid(formGroup, n)
          ? VALIDATION_CLASSES.hidden
          : VALIDATION_CLASSES.visible,
        inputClasses: this.isFormFieldValid(formGroup, n)
          ? ''
          : VALIDATION_CLASSES.inputErr,
        iconClasses: this.isFormFieldValid(formGroup, n)
          ? VALIDATION_CLASSES.iconHidden
          : VALIDATION_CLASSES.iconVisible,
        msgClasses: this.isFormFieldValid(formGroup, n)
          ? VALIDATION_CLASSES.msgHidden
          : VALIDATION_CLASSES.msgVisible,
      };
      validationStyles[n] = styleObj;
    }

    return validationStyles;
  }

  capitalizeFirst(str: string): string {
    return str.substring(0, 1).toUpperCase() + str.substring(1);
  }

  buildErrMsgMap(controls: { [key: string]: AbstractControl<any, any> }): {
    [key: string]: string;
  } {
    const errMsgMap: { [key: string]: string } = {};
    for (const c in controls) {
      if (controls[c].errors !== null) {
        const controlErrs = controls[c].errors;
        for (const e in controlErrs) {
          switch (e) {
            case 'required':
              errMsgMap[c] = this.capitalizeFirst(`${c} is a required field`);
              break;
            case 'minlength':
              const min = controlErrs[e].requiredLength;
              errMsgMap[c] = this.capitalizeFirst(
                `${c} must be at least ${min} characters long`
              );
              break;
            default:
              errMsgMap[c] = 'Unspecified validation error';
          }
        }
      }
    }
    return errMsgMap;
  }
}
