import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { getCookie } from 'typescript-cookie';

@Injectable()
export class HttpService {
  handleHttpErrors(e: HttpErrorResponse) {
    console.log('e:', e);
    return throwError(() => new Error('an unspecified error'));
  }

  setXsrfToken() {
    if (!getCookie('XSRF-TOKEN')) {
      throw new Error('XSRF-TOKEN not found');
    }
    if (!window.sessionStorage.getItem('XSRF-TOKEN')) {
      window.sessionStorage.setItem('XSRF-TOKEN', getCookie('XSRF-TOKEN')!);
    }
  }
}
