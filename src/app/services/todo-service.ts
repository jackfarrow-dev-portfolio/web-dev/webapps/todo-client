import { EventEmitter, Injectable } from '@angular/core';
import { Todo } from '../models/todo';
import data from '../data/todos-data.json';

@Injectable()
export class TodoService {
  todos: Todo[] = [];
  selectedTodo!: Todo;

  constructor() {
    data.forEach((d) => {
      this.todos.push({
        userId: d.userId,
        todoId: d.todoId,
        createdDateTime: d.createdDateTime,
        completedDateTime: d.completedDateTime ? d.completedDateTime : null,
        title: d.title,
        status: d.status,
        isComplete: d.isComplete,
        description: d.description,
      } as Todo);
    });
  }

  deserializeTodo(todoData: any) {
    return {
      userId: todoData.userId || '',
      todoId: todoData.todoId || '',
      createdDateTime: todoData.createdDateTime || '',
      completedDateTime: todoData.completedDateTime || null,
      title: todoData.title || '',
      description: todoData.description || '',
      status: todoData.status || '',
    } as Todo;
  }

  getTodosByUserId(userId: string): Promise<Todo[]> {
    return Promise.resolve(
      this.todos.filter((todo: Todo) => todo.userId === userId)
    );
  }

  setSelectedTodo(todo: Todo) {
    this.selectedTodo = todo;
  }

  createTodo(todo: Todo) {
    this.todos.push(todo);
  }

  deleteTodoByTodoId(todoId: string) {
    const idx = this.todos.findIndex((t: Todo) => t.todoId === todoId);
    this.todos.splice(idx, 1);
  }

  updateTodo(todo: Todo): Promise<Todo[]> {
    const idx = this.todos.findIndex((t: Todo) => t.todoId === todo.todoId);
    this.todos[idx] = todo;
    return Promise.resolve(this.todos);
  }
}
