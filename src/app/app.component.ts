import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'todos-client';
  userId!: string;
  isModalOpen = false;

  constructor(private activatedRoute: ActivatedRoute) {}

  onModalClosed(modalOpenStatus: boolean) {
    console.log(`modalOpenStatus: ${modalOpenStatus}`);
    this.isModalOpen = modalOpenStatus;
  }

  ngOnInit(): void {
    this.userId = 'e6db304f-07fa-40ba-8411-f1a330aad7bd';
  }
}
