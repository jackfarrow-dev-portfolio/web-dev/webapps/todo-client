const HTTP_CONFIG = {
  baseUrl: 'http://localhost:8080',
  apiPath: 'api/v1',
  loginRoute: 'auth/login',
  signupRoute: 'auth/signup',
};

export default HTTP_CONFIG;
