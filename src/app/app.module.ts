import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { LoginService } from './services/login-service';
import { TodoService } from './services/todo-service';
import { HttpService } from './services/http-service';
import { ValidatorService } from './services/validator-service';
import { TodosListComponent } from './components/todos/todos-list/todos-list.component';
import { TodoDetailsComponent } from './components/todos/todo-details/todo-details.component';
import { CreateTodoComponent } from './components/todos/create-todo/create-todo.component';
import { EditTodoComponent } from './components/todos/edit-todo/edit-todo.component';
import { SignupComponent } from './components/signup/signup.component';
import { TestformComponent } from './components/testform/testform.component';
import { XhrInterceptor } from './interceptors/xhr-interceptor';
import { HeaderComponent } from './components/header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ModalComponent } from './components/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TodosListComponent,
    TodoDetailsComponent,
    CreateTodoComponent,
    EditTodoComponent,
    SignupComponent,
    TestformComponent,
    HeaderComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule,
  ],
  providers: [
    LoginService,
    TodoService,
    HttpService,
    ValidatorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: XhrInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
